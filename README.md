# Scraping

A LinkedIn scraping engine for Asia Tech Venture

**You must have the following program installed:**

1. Git

Check if you have git installed through cmd:
> git --version

It chould return your git version. If not, then install here: https://git-scm.com/downloads

2. Python 

Check if you have python installed through cmd:
> python --version

It chould return your python version. If not, then install python here: https://www.python.org/

3. Pip
Type this on cmd to install necessary packages: 

> python -m pip install --upgrade pip setuptools wheel
>
> pip install selenium
>
> pip install numpy
>
> pip install time
>
> pip install pprint

4. *If you got error after running the code like this: selenium.common.exceptions.WebDriverException or something about the chromedriver, then ensure that you are using the matching version to your chrome browser. Download chromedriver here, and copy the chromedriver.exe into this directory (override the existing one): 
 https://chromedriver.chromium.org/downloads

**Run the code:**

1. Open the command prompt or other command shell

2. Clone this project

Type this inside your cmd: 
>git clone https://gitlab.com/fathinah/scraping.git

3. Go into the repository

> cd scraping

4. Configure your linkedin email and password inside file parameters.py

>linkedin_username = 'your username'
>
>linkedin_password = 'your password'

5. Run the app (make sure @cmd that your current directory is scraping)

> python script.py