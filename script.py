from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from time import sleep
from parameters import linkedin_username, linkedin_password, profiles
import numpy as np
import csv
import pandas as pd

result = []
url = ""
profile={'name':[], 'contacts':[], 'location':[], 'skills':[], 'interests':[]}
def linkedin_login():
    '''
    Function to do automated login into linkedin 
    '''
    global driver
    chrome_options = webdriver.ChromeOptions()
    # uncomment the following code to run a headless mode ( non-appearing scraping )
    # chrome_options.add_argument('--headless')
    chrome_options.add_argument("start-maximized")
    chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
    chrome_options.add_experimental_option("detach", True)

    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(chrome_options=chrome_options, executable_path="chromedriver")
 
    driver.get('https://www.linkedin.com')
    # locate email form by_id
    username = driver.find_element_by_id('session_key')
    # send_keys() to simulate key strokes
    username.send_keys(linkedin_username)
    # sleep for 0.5 seconds
    sleep(0.5)
    # locate password form by_id
    password = driver.find_element_by_id('session_password')
    # send_keys() to simulate key strokes
    password.send_keys(linkedin_password)
    sleep(0.5)
    # locate submit button by_class_name
    sign_in_button = driver.find_element_by_class_name("sign-in-form__submit-button")
    sleep(0.5)
    # .click() to mimic button click
    sign_in_button.click()
    sleep(3)

def scroll():
    '''
    Function to scroll down the page to load it properly
    '''
    for i in np.arange(0.0, 1.0, 0.1):
        scroll_to = "window.scrollTo(0, document.body.scrollHeight *" + str(i) + ")"
        driver.execute_script(scroll_to)
        sleep(0.5)

def scrape(url):
    '''
    Function to scrape profile elements
    '''
    
    driver.get(url+"detail/contact-info/")
    sleep(1)
    contacts = WebDriverWait(driver, 5).until(EC.presence_of_all_elements_located((By.CLASS_NAME, 'pv-contact-info__contact-link')))
    profile_links = [item.get_attribute('href') for item in contacts]
    sleep(0.5)
    profile['contacts']+= [profile_links]
    
    actions = ActionChains(driver)
    actions.move_by_offset(0, 0).click().perform()

    name = driver.find_element_by_css_selector("li.inline.t-24.t-black.t-normal.break-words")
    profile['name']+= [name.text]
    location = driver.find_element_by_css_selector('ul.pv-top-card--list-bullet li')
    sleep(1)
    profile['location']+= [location.text]
    
    try: 
        scroll()
        a = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.CLASS_NAME, 'pv-skill-categories-section')))
        driver.execute_script("arguments[0].scrollIntoView();", a)
        sleep(3)
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.CLASS_NAME, 'pv-skills-section__additional-skills'))).click()
        skills = WebDriverWait(driver, 5).until(EC.presence_of_all_elements_located((By.CLASS_NAME, 'pv-skill-category-entity__name-text')))
        sleep(2)
        user_skills = [skill.text for skill in skills]
        profile['skills']+=[[user_skills]]
    except:
        profile['skills']+=[['']]
    scroll()
    driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
    sleep(1)
    interests = driver.find_elements_by_class_name("pv-entity__summary-title-text")
    user_interests = [interest.text for interest in interests]
    sleep(1)
    profile['interests']+= [[user_interests]]


def set_url(keyword, connections, ci, locations, industries):
    '''
    Function to set up search keyword and filter out the result
    '''
    terms = keyword.replace(' ','%20')
    url = "https://www.linkedin.com/search/results/people/?keywords="+keyword+"&origin=SWITCH_SEARCH_VERTICAL"
    driver.get(url)
    sleep(1)
    WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'button.search-filters-bar__all-filters'))).click()
    sleep(2)
    for i in connections:
        if i=='1':
            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, "//label[contains(..,'1st')]"))).click()
        elif i=='2':
            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, "//label[contains(..,'2nd')]"))).click()
        elif i=='3':
            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, "//label[contains(..,'3rd+')]"))).click()
    for j in ci:
        if j == '1':
            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, "//label[contains(..,'Probono consulting and volunteering')]"))).click()
        elif j=='2':
            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, "//label[contains(..,'Joining a nonprofit board')]"))).click()

    if locations != ['']:
        elem = WebDriverWait(driver,10).until(EC.visibility_of_element_located((By.XPATH, "//input[@placeholder='Add a country/region']")))
        for k in locations:
            elem.send_keys(k)
            sleep(2)
            elem.send_keys(Keys.ARROW_DOWN)
            sleep(1)
            elem.send_keys(Keys.ENTER)
    if industries != ['']:
        elem_industry = WebDriverWait(driver,10).until(EC.visibility_of_element_located((By.XPATH, "//input[@placeholder='Add an industry']")))
        for l in industries:
            elem_industry.send_keys(l)
            sleep(2)
            elem_industry.send_keys(Keys.ARROW_DOWN)
            sleep(1)
            elem_industry.send_keys(Keys.ENTER)
    
    WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'button.search-advanced-facets__button--apply'))).click()

def search_term():
    '''
    Function to grab all profile url from the search result
    '''
    profiles = WebDriverWait(driver, 5).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, '.search-result__info > .search-result__result-link')))
    list_of_urls = [profile.get_attribute("href") for profile in profiles if 'search/results/people/?' not in profile.get_attribute("href") ]
    for url in list_of_urls:
        res = scrape(url)
        result.append(res)

#program starts
if len(profiles) == 0:
    keyword = input("What is the profile keyword you are looking for?")
    sum_pages = int(input("How many pages you want to scrap?(e.g. 3)"))
    # 1 for first type connection, 2 for 2nd connections, 3 for 3rd connections
    connections = input("What connections you are looking?(e.g. 1,2,3 - leave blank for default)").split(',')
    ci = input("Contact interests (`1`: Probono consulting and volunteering, `2`: Joining a nonprofit board)(leave blank for default)").split(',')
    locations = input("Locations: (e.g. Singapore,Jakarta)").split(',')
    industries = input("Industry : (check for more https://developer.linkedin.com/docs/reference/industry-codes)").split(',')
    linkedin_login()
    set_url(keyword, connections, ci, locations, industries)
    sleep(2)
    url = driver.current_url
    print(url)
    for i in range(sum_pages):
        this_url= url+"&page="+str(i+1)
        driver.get(this_url)
        sleep(1)
        scroll()
        search_term()
else:
    linkedin_login()
    for link in profiles:
        profile = scrape(link)

print(profile)
driver.close()
df=pd.DataFrame(data=profile)
df.to_csv("result.csv", mode='a', header=False)

print("scrap result is added to result.csv")
